# Setting microservice

This microservice provides settings for different microservices in the Applicant Tracking Project. Currently, provides configuration options for users who want to manage their notifications settings.

Actual set port for this microservices is `http://localhost:8686/`

## UserSettings

### Endpoints enabled

* **Get UserSetting by user ID and Roll Setting ID:** Get all the settings with its values from a User using the endpoint `GET /api/v1/settings`
* **Update or save:** Updates or save a UserSetting using endpoint `POST /api/v1/settings/`

```
{
		"userId": "21ffc534-1d83-44d5-b264-1e17feabd322",
		"value": "true",
		"settingId": "40d56db6-f717-11ec-b939-0242ac120002"
}
```

You can see more about the API documentation with [Swagger](http://localhost:8686/swagger-ui/index.html).
