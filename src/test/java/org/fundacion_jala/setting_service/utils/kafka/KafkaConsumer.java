package org.fundacion_jala.setting_service.utils.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.fundacion_jala.setting_service.events.UserSettingCreationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);
  public static final ObjectMapper JSON_MAPPER = new ObjectMapper();

  private CountDownLatch latch = new CountDownLatch(1);
  private UserSettingCreationEvent payload;

  @KafkaListener(topics = "${test.topic}")
  public void receive(ConsumerRecord<String, UserSettingCreationEvent> consumerRecord) {
    String json = String.valueOf(consumerRecord.value());
    try {
      payload = JSON_MAPPER.readValue(json, UserSettingCreationEvent.class);
    } catch (IOException e) {
      LOGGER.error("Error in the deserialization process. Details: " + e.getMessage());
    }
    latch.countDown();
  }

  public CountDownLatch getLatch() {
    return latch;
  }

  public UserSettingCreationEvent getPayload() {
    return payload;
  }
}
