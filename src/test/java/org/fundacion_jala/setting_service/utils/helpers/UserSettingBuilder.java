package org.fundacion_jala.setting_service.utils.helpers;

import java.util.UUID;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.responses.UserSettingResponse;

public class UserSettingBuilder {

  public static UserSetting buildUserSetting() {
    UserSetting userSetting = new UserSetting();
    userSetting.setSetting(SettingBuilder.buildSetting());
    userSetting.setValue("false");
    userSetting.setUserId(UUID.randomUUID());
    return userSetting;
  }

  public static UserSetting buildUserSetting(Setting setting) {
    UserSetting userSetting = buildUserSetting();
    userSetting.setSetting(setting);

    return userSetting;
  }

  public static UserSetting buildUserSetting(UUID settingId, UUID userId, String settingValue) {
    UserSetting userSetting = new UserSetting();
    userSetting.setId(settingId);
    userSetting.setUserId(userId);
    userSetting.setValue(settingValue);

    return userSetting;
  }

  public static UserSettingResponse buildUserSettingResponse(
      UUID settingId, UUID userId, String settingValue) {
    UserSettingResponse userSettingResponse = new UserSettingResponse();
    userSettingResponse.setSettingId(settingId);
    userSettingResponse.setValue(settingValue);
    userSettingResponse.setUserId(userId);

    return userSettingResponse;
  }
}
