package org.fundacion_jala.setting_service.utils.helpers;

import java.util.UUID;
import org.fundacion_jala.setting_service.models.SectionSetting;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.enums.SettingDataType;

public class SettingBuilder {

  private static final SectionSetting TYPE_SETTING = new SectionSetting();

  public static Setting buildSetting() {
    Setting setting = new Setting();
    setting.setSettingDataType(SettingDataType.BOOLEAN);
    setting.setDescription("This is a description");

    return setting;
  }

  public static Setting buildSetting(UUID settingId) {
    Setting newSetting = buildSetting();
    newSetting.setId(settingId);
    newSetting.setTypeSetting(TYPE_SETTING);

    return newSetting;
  }
}
