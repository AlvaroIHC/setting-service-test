package org.fundacion_jala.setting_service.utils.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.RoleSetting;
import org.fundacion_jala.setting_service.models.Setting;

public class RoleSettingBuilder {

  public static RoleSetting buildRoleSetting() {
    RoleSetting roleSetting = new RoleSetting();
    roleSetting.setSetting(SettingBuilder.buildSetting());
    roleSetting.setRoleId(UUID.randomUUID());
    return roleSetting;
  }

  public static RoleSetting buildRoleSetting(Setting setting) {
    RoleSetting roleSetting = buildRoleSetting();
    roleSetting.setSetting(setting);

    return roleSetting;
  }

  public static RoleSetting buildRoleSetting(UUID roleSettingId, UUID roleId, Setting settingObj) {
    RoleSetting roleSetting = new RoleSetting();
    roleSetting.setId(roleSettingId);
    roleSetting.setRoleId(roleId);
    roleSetting.setSetting(settingObj);

    return roleSetting;
  }

  public static List<RoleSetting> buildRoleSettingList(int amount) {
    List<RoleSetting> roleSettings = new ArrayList<>();
    Setting setting = SettingBuilder.buildSetting();
    while (amount > 0) {
      roleSettings.add(buildRoleSetting(setting));
      amount--;
    }
    return roleSettings;
  }

  public static List<RoleSetting> buildRoleSettingList(
      UUID roleId, Setting settingObj, int amount) {
    List<RoleSetting> roleSettingList = new ArrayList<>();

    while (amount > 0) {
      RoleSetting roleSetting = buildRoleSetting(UUID.randomUUID(), roleId, settingObj);

      roleSettingList.add(roleSetting);
      amount--;
    }
    return roleSettingList;
  }
}
