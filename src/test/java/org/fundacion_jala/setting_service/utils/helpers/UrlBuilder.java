package org.fundacion_jala.setting_service.utils.helpers;

import java.util.HashMap;
import java.util.Map;

public class UrlBuilder {

  private static final String EQUALS_SIGN = "=";
  private static final String QUESTION_MARK = "?";
  private static final String AMPERSAND = "&";
  private String baseUrl;
  private Map<String, String> parameters;

  public UrlBuilder(String url) {
    this.baseUrl = url;
    this.parameters = new HashMap<>();
  }

  public UrlBuilder addParameter(String key, String value) {
    parameters.put(key, value);
    return this;
  }

  @Override
  public String toString() {
    String url = this.baseUrl.concat(QUESTION_MARK);
    for (Map.Entry<String, String> entry : parameters.entrySet()) {
      url += entry.getKey().concat(EQUALS_SIGN) + entry.getValue().concat(AMPERSAND);
    }
    return url;
  }
}
