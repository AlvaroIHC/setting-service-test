package org.fundacion_jala.setting_service.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.utils.helpers.SettingBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class SettingRepositoryTest {

  @Autowired private SettingRepository settingRepository;
  @Autowired private TestEntityManager entityManager;
  private Setting setting;

  @BeforeEach
  public void beforeAll() {
    setting = SettingBuilder.buildSetting();
  }

  @Test
  public void shouldFindNoRoleSettingsIfRepositoryIsEmpty() {
    List<Setting> settings = settingRepository.findAll();
    assertThat(settings).isEmpty();
  }

  @Test
  public void shouldStoreASetting() {
    Setting storedSetting = settingRepository.save(setting);
    assertThat(storedSetting)
        .hasFieldOrPropertyWithValue("settingDataType", setting.getSettingDataType());
    assertThat(storedSetting).hasFieldOrPropertyWithValue("typeSetting", setting.getTypeSetting());
    assertThat(storedSetting).hasFieldOrPropertyWithValue("description", setting.getDescription());
  }

  @Test
  public void shouldFindASettingById() {
    UUID settingId = entityManager.persist(setting).getId();
    Optional<Setting> wrapperRetrievedSetting = settingRepository.findById(settingId);
    assertThat(wrapperRetrievedSetting).isPresent();
  }

  @Test
  public void shouldUpdateASetting() {
    UUID settingId = entityManager.persist(setting).getId();
    Optional<Setting> wrapperRetrievedSetting = settingRepository.findById(settingId);
    Setting settingBeforeUpdate = new Setting();
    if (wrapperRetrievedSetting.isPresent()) {
      settingBeforeUpdate = wrapperRetrievedSetting.get();
    }
    assertThat(settingBeforeUpdate)
        .hasFieldOrPropertyWithValue("description", "This is a description");

    setting.setDescription("This is an updated description");
    settingRepository.save(setting);

    wrapperRetrievedSetting = settingRepository.findById(settingId);
    Setting settingAfterUpdate = new Setting();
    if (wrapperRetrievedSetting.isPresent()) {
      settingAfterUpdate = wrapperRetrievedSetting.get();
    }
    assertThat(settingAfterUpdate)
        .hasFieldOrPropertyWithValue("description", "This is an updated description");
  }
}
