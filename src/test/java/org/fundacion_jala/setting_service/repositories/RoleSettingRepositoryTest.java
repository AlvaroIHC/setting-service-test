package org.fundacion_jala.setting_service.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.RoleSetting;
import org.fundacion_jala.setting_service.utils.helpers.RoleSettingBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class RoleSettingRepositoryTest {

  @Autowired private RoleSettingRepository roleSettingRepository;

  @Autowired private TestEntityManager entityManager;

  @Test
  public void shouldFindNoRoleSettingsIfRepositoryIsEmpty() {
    List<RoleSetting> roleSettings = roleSettingRepository.findAll();
    assertThat(roleSettings).isEmpty();
  }

  @Test
  public void shouldFindTenRoleSettingRecords() {
    int amount = 10;
    List<RoleSetting> roleSettings = RoleSettingBuilder.buildRoleSettingList(amount);
    entityManager.persist(roleSettings.get(0).getSetting());
    roleSettings.forEach(roleSetting -> entityManager.persist(roleSetting));

    List<RoleSetting> retrievedRoleSettings = roleSettingRepository.findAll();
    assertThat(roleSettings).hasSize(amount);
  }

  @Test
  public void shouldFindRoleSettingsByRoleId() {

    RoleSetting roleSetting = RoleSettingBuilder.buildRoleSetting();
    UUID roleId = roleSetting.getRoleId();
    entityManager.persist(roleSetting.getSetting());
    entityManager.persist(roleSetting);

    List<RoleSetting> roleSettings = roleSettingRepository.findAllByRoleId(roleId);
    assertThat(roleSettings).hasSize(1);
    RoleSetting result = roleSettings.get(0);
    assertThat(result).hasFieldOrPropertyWithValue("setting", roleSetting.getSetting());
  }
}
