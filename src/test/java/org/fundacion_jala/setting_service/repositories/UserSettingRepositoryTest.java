package org.fundacion_jala.setting_service.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.utils.helpers.SettingBuilder;
import org.fundacion_jala.setting_service.utils.helpers.UserSettingBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
public class UserSettingRepositoryTest {

  @Autowired private SettingRepository settingRepository;

  @Autowired UserSettingRepository userSettingRepository;

  @Autowired private TestEntityManager entityManager;

  private Setting setting;

  @BeforeEach
  public void beforeAll() {
    setting = SettingBuilder.buildSetting();
  }

  @Test
  public void shouldFindNoUserSettingsIfRepositoryIsEmpty() {
    Iterable<UserSetting> users = userSettingRepository.findAll();
    assertThat(users).isEmpty();
  }

  @Test
  public void shouldStoreValueOfUserSetting() {
    UserSetting userSetting = UserSettingBuilder.buildUserSetting();
    entityManager.persist(userSetting.getSetting());
    entityManager.persist(userSetting);

    UserSetting storedUserSetting = userSettingRepository.save(userSetting);

    assertThat(storedUserSetting).hasFieldOrPropertyWithValue("value", userSetting.getValue());
  }

  @Test
  public void shouldFindByUserIdAndSettingId() {
    UUID settingId = entityManager.persist(setting).getId();

    UserSetting userSetting = UserSettingBuilder.buildUserSetting(setting);
    UUID userId = userSetting.getUserId();
    entityManager.persist(userSetting.getSetting());
    entityManager.persist(userSetting);

    Optional<UserSetting> wrapperRetrievedUserSetting =
        userSettingRepository.findByUserIdAndSettingId(userId, settingId);
    assertThat(wrapperRetrievedUserSetting).isPresent();
  }
}
