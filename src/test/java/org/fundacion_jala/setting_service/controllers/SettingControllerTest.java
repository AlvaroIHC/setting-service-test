package org.fundacion_jala.setting_service.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.SectionSetting;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.models.enums.SettingDataType;
import org.fundacion_jala.setting_service.responses.SettingResponse;
import org.fundacion_jala.setting_service.responses.UserSettingResponse;
import org.fundacion_jala.setting_service.services.SettingService;
import org.fundacion_jala.setting_service.utils.constants.EndPoint;
import org.fundacion_jala.setting_service.utils.helpers.UrlBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(SettingController.class)
public class SettingControllerTest {

  public static final String JSON_ROOT = "$";
  public static final String SETTING_RESPONSE_DESCRIPTION = "Testing setting response description";
  public static final UUID UUID1 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7d");
  public static final UUID UUID2 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7a");
  public static final String USER_ID = "userId";
  public static final String ROLE_ID = "roleId";
  SectionSetting TYPE_SETTING = new SectionSetting();

  @Autowired MockMvc mockMvc;
  @Autowired ObjectMapper mapper;

  @MockBean SettingService settingService;

  SettingResponse settingResponse1 =
      new SettingResponse(
          UUID1, TYPE_SETTING, SettingDataType.BOOLEAN, SETTING_RESPONSE_DESCRIPTION, true);
  SettingResponse settingResponse2 =
      new SettingResponse(
          UUID2, TYPE_SETTING, SettingDataType.BOOLEAN, SETTING_RESPONSE_DESCRIPTION, true);

  @Test
  public void findByUserIdAndSettingIdTest() {
    String url =
        new UrlBuilder(EndPoint.SETTINGS)
            .addParameter(ROLE_ID, UUID1.toString())
            .addParameter(USER_ID, UUID2.toString())
            .toString();

    List<SettingResponse> settings =
        new ArrayList<>(Arrays.asList(settingResponse1, settingResponse2));
    Mockito.when(settingService.getSettingsByRoleIdAndUserId(UUID1, UUID2)).thenReturn(settings);
    try {
      mockMvc
          .perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath(JSON_ROOT, hasSize(2)))
          .andExpect(jsonPath(JSON_ROOT + "[1].description", is(SETTING_RESPONSE_DESCRIPTION)));
    } catch (Exception e) {
      fail();
    }
  }

  @Test
  public void saveUserSettingTest() {
    UserSettingResponse userSettingResponse = new UserSettingResponse();
    userSettingResponse.setSettingId(UUID.randomUUID());
    userSettingResponse.setValue("true");
    userSettingResponse.setUserId(UUID.randomUUID());
    UserSetting userSetting = userSettingResponse.toUserSetting(new Setting());
    Mockito.when(settingService.saveUserSetting(userSettingResponse)).thenReturn(userSetting);
    try {
      MockHttpServletRequestBuilder mockRequest =
          MockMvcRequestBuilders.post(EndPoint.SETTINGS)
              .contentType(MediaType.APPLICATION_JSON)
              .accept(MediaType.APPLICATION_JSON)
              .content(this.mapper.writeValueAsString(userSettingResponse));

      mockMvc
          .perform(mockRequest)
          .andExpect(status().isOk())
          .andExpect(jsonPath(JSON_ROOT, notNullValue()))
          .andExpect(jsonPath(JSON_ROOT + ".value", is("true")));
    } catch (Exception e) {
      fail();
    }
  }
}
