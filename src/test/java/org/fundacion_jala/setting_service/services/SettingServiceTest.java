package org.fundacion_jala.setting_service.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.RoleSetting;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.repositories.RoleSettingRepository;
import org.fundacion_jala.setting_service.repositories.SettingRepository;
import org.fundacion_jala.setting_service.repositories.UserSettingRepository;
import org.fundacion_jala.setting_service.responses.SettingResponse;
import org.fundacion_jala.setting_service.responses.UserSettingResponse;
import org.fundacion_jala.setting_service.utils.helpers.RoleSettingBuilder;
import org.fundacion_jala.setting_service.utils.helpers.SettingBuilder;
import org.fundacion_jala.setting_service.utils.helpers.UserSettingBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SettingServiceTest {

  public static final UUID UUID1 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7d");
  public static final UUID UUID2 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7a");
  public static final UUID UUID3 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7b");
  public static final UUID UUID4 = UUID.fromString("fc07c813-e15c-46e4-8a3f-9d001fd26f7c");

  public static final int ROLE_LIST_LENGTH = 10;

  @Mock private SettingRepository settingRepository;

  @Mock private RoleSettingRepository roleSettingRepository;

  @Mock private UserSettingRepository userSettingRepository;

  @InjectMocks private SettingService service;

  @Test
  public void getSettingsByRoleIdAndUserIdTest() {
    // Setting object
    Setting testSetting = SettingBuilder.buildSetting(UUID3);

    // User Setting
    UserSetting testUserSetting = UserSettingBuilder.buildUserSetting(UUID3, UUID4, "true");

    List<RoleSetting> roleSettings =
        RoleSettingBuilder.buildRoleSettingList(UUID1, testSetting, ROLE_LIST_LENGTH);

    Mockito.when(roleSettingRepository.findAllByRoleId(any(UUID.class))).thenReturn(roleSettings);
    when(userSettingRepository.findByUserIdAndSettingId(any(UUID.class), any(UUID.class)))
        .thenReturn(Optional.of(testUserSetting));

    List<SettingResponse> settingResponses = service.getSettingsByRoleIdAndUserId(UUID1, UUID4);

    assertNotNull(settingResponses);
    assertEquals(settingResponses.size(), ROLE_LIST_LENGTH);
    assertEquals(UUID3, settingResponses.get(7).getId());
    assertTrue(settingResponses.get(3).isValue());
  }

  @Test
  public void saveUserSettingTest() {
    // User Setting Response
    UserSettingResponse testUserSettingResponse =
        UserSettingBuilder.buildUserSettingResponse(UUID1, UUID3, "true");

    // Setting
    Setting existingSetting = SettingBuilder.buildSetting(UUID1);

    when(settingRepository.findById(any(UUID.class))).thenReturn(Optional.of(existingSetting));
    when(userSettingRepository.findByUserIdAndSettingId(any(UUID.class), any(UUID.class)))
        .thenReturn(Optional.empty());
    when(userSettingRepository.save(any(UserSetting.class)))
        .thenReturn(testUserSettingResponse.toUserSetting(existingSetting));

    UserSetting savedUserSetting = service.saveUserSetting(testUserSettingResponse);

    assertNotNull(savedUserSetting);
    assertEquals(UUID1, savedUserSetting.getSetting().getId());
    assertEquals("true", savedUserSetting.getValue());
  }
}
