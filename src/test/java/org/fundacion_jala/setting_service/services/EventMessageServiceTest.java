package org.fundacion_jala.setting_service.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.utils.helpers.UserSettingBuilder;
import org.fundacion_jala.setting_service.utils.kafka.KafkaConsumer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(
    partitions = 1,
    brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"})
class EventMessageServiceTest {

  @Autowired private EventMessageService producer;

  @Autowired private KafkaConsumer consumer;

  @Value("${test.topic}")
  private String topic;

  @Test
  void sendUserSettingMessage() throws Exception {
    UserSetting userSetting = UserSettingBuilder.buildUserSetting();
    producer.sendUserSettingMessage(userSetting);
    boolean messageConsumed = consumer.getLatch().await(10, TimeUnit.SECONDS);
    assertTrue(messageConsumed);
    assertEquals(consumer.getPayload().getData().getUserId(), userSetting.getUserId());
  }
}
