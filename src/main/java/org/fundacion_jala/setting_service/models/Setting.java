package org.fundacion_jala.setting_service.models;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.setting_service.models.enums.SettingDataType;
import org.fundacion_jala.setting_service.utils.constants.Defaults;
import org.fundacion_jala.setting_service.utils.constants.TableConstants;
import org.fundacion_jala.setting_service.utils.validations.SettingValidator;
import org.hibernate.annotations.Type;

@Entity
@Getter
@Setter
@Table(name = TableConstants.TABLE_NAME_SETTING)
@EqualsAndHashCode(of = {"id", "typeSetting", "settingDataType"})
public class Setting {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID)
  private UUID id;

  @ManyToOne
  @JoinColumn(name = TableConstants.TABLE_FOREIGN_TYPE_SETTING_ID)
  private SectionSetting typeSetting;

  @Enumerated(EnumType.STRING)
  private SettingDataType settingDataType;

  @Size(
      min = Defaults.MIN_STRING_LENGTH,
      max = Defaults.MAX_STRING_LENGTH,
      message = SettingValidator.VALIDATION_LENGTH_VALUE)
  @NotBlank(message = SettingValidator.NOT_BLANK_MESSAGE)
  @Pattern(regexp = "^[a-zA-Z\\s]*", message = SettingValidator.VALIDATION_PATTERN_DESCRIPTION)
  private String description;
}
