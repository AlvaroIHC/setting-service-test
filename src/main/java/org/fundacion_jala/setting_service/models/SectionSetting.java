package org.fundacion_jala.setting_service.models;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.setting_service.utils.constants.Defaults;
import org.fundacion_jala.setting_service.utils.constants.TableConstants;
import org.fundacion_jala.setting_service.utils.validations.TypeSettingValidator;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
@Table(name = TableConstants.TABLE_NAME_TYPE_SETTING)
@EqualsAndHashCode(of = {"id", "name"})
public class SectionSetting {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID)
  private UUID id;

  @NotBlank(message = TypeSettingValidator.NOT_BLANK_MESSAGE_NAME)
  @Pattern(regexp = "^[a-zA-Z\\s]*", message = TypeSettingValidator.VALIDATION_PATTERN_NAME)
  @Size(
      min = Defaults.MIN_STRING_LENGTH,
      max = Defaults.MAX_STRING_LENGTH,
      message = TypeSettingValidator.VALIDATION_LENGTH_DESCRIPTION)
  @Column
  private String name;

  @Size(
      min = Defaults.MIN_STRING_LENGTH,
      max = Defaults.MAX_STRING_LENGTH,
      message = TypeSettingValidator.VALIDATION_LENGTH_DESCRIPTION)
  @NotBlank(message = TypeSettingValidator.NOT_BLANK_MESSAGE_DESCRIPTION)
  @Pattern(regexp = "^[a-zA-Z\\s]*", message = TypeSettingValidator.VALIDATION_PATTERN_DESCRIPTION)
  private String description;
}
