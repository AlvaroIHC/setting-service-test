package org.fundacion_jala.setting_service.models;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.setting_service.utils.constants.Defaults;
import org.fundacion_jala.setting_service.utils.constants.TableConstants;
import org.fundacion_jala.setting_service.utils.validations.UserSettingValidator;
import org.hibernate.annotations.Type;

@Setter
@Getter
@Entity
@Table(name = TableConstants.TABLE_NAME_USER_SETTING)
@EqualsAndHashCode(of = {"id", "userId", "setting"})
public class UserSetting {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID)
  private UUID id;

  @Type(type = TableConstants.TYPE_UUID)
  private UUID userId;

  @ManyToOne
  @JoinColumn(name = TableConstants.TABLE_FOREIGN_SETTING_ID)
  private Setting setting;

  @Size(
      min = Defaults.MIN_STRING_LENGTH,
      max = Defaults.MAX_STRING_LENGTH,
      message = UserSettingValidator.VALIDATION_LENGTH_VALUE)
  @NotNull(message = UserSettingValidator.NOT_NULL_MESSAGE)
  private String value;
}
