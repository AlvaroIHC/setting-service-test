package org.fundacion_jala.setting_service.models.kafka;

import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class UserSettingDTO {
  private UUID userId;
  private boolean value;
  private UUID settingId;
}
