package org.fundacion_jala.setting_service.models;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.setting_service.utils.constants.TableConstants;
import org.hibernate.annotations.Type;

@Entity
@Setter
@Getter
@Table(name = TableConstants.TABLE_NAME_SETTING_ROLE)
@EqualsAndHashCode(of = {"id", "roleId", "setting"})
public class RoleSetting {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID)
  private UUID id;

  @Type(type = TableConstants.TYPE_UUID)
  private UUID roleId;

  @ManyToOne
  @JoinColumn(name = TableConstants.TABLE_FOREIGN_SETTING_ID)
  private Setting setting;
}
