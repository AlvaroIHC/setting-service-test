package org.fundacion_jala.setting_service.services;

import static org.fundacion_jala.setting_service.utils.constants.TableConstants.TABLE_NAME_SETTING;
import static org.fundacion_jala.setting_service.utils.constants.TableConstants.TABLE_NAME_USER_SETTING;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.apache.kafka.common.errors.TimeoutException;
import org.fundacion_jala.setting_service.models.RoleSetting;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.models.enums.SettingDataType;
import org.fundacion_jala.setting_service.repositories.RoleSettingRepository;
import org.fundacion_jala.setting_service.repositories.SettingRepository;
import org.fundacion_jala.setting_service.repositories.UserSettingRepository;
import org.fundacion_jala.setting_service.responses.SettingResponse;
import org.fundacion_jala.setting_service.responses.UserSettingResponse;
import org.fundacion_jala.setting_service.utils.validations.exceptions.EntityNotFoundException;
import org.fundacion_jala.setting_service.utils.validations.exceptions.EntityUnableToStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SettingService {

  private final boolean DEFAULT_USER_SETTING_VALUE = false;
  @Autowired private RoleSettingRepository roleSettingRepository;

  @Autowired private UserSettingRepository userSettingRepository;

  @Autowired private SettingRepository settingRepository;
  @Autowired private EventMessageService eventMessageService;

  public List<SettingResponse> getSettingsByRoleIdAndUserId(UUID roleId, UUID userId) {
    List<RoleSetting> roleSettings = roleSettingRepository.findAllByRoleId(roleId);
    List<SettingResponse> settingsResponse =
        roleSettings.stream()
            .filter(
                roleSetting ->
                    roleSetting.getSetting().getSettingDataType().equals(SettingDataType.BOOLEAN))
            .map(
                roleSetting -> {
                  Optional<UserSetting> userSetting =
                      userSettingRepository.findByUserIdAndSettingId(
                          userId, roleSetting.getSetting().getId());
                  boolean value =
                      userSetting.isPresent()
                          ? Boolean.valueOf(userSetting.get().getValue())
                          : DEFAULT_USER_SETTING_VALUE;
                  SettingResponse settingResponse =
                      new SettingResponse(
                          roleSetting.getSetting().getId(),
                          roleSetting.getSetting().getTypeSetting(),
                          roleSetting.getSetting().getSettingDataType(),
                          roleSetting.getSetting().getDescription(),
                          value);
                  return settingResponse;
                })
            .collect(Collectors.toList());
    return settingsResponse;
  }

  /**
   * Returns a UserSetting object when that object is saved. If an exception occurs when a Kafka
   * Event tries to send a message a rollback is called and the object is not saved. If the type of
   * Setting is not storaged throws an {@link EntityNotFoundException} And if any field is invalid
   * throws an {@link EntityUnableToStorageException}
   *
   * @param userSettingResponse
   * @return the saved entity type UserSetting; will never be null.
   * @throws EntityNotFoundException in case the setting attribute is null.
   * @throws EntityUnableToStorageException in case the userSetting to save was not storaged
   */
  @Transactional(rollbackFor = TimeoutException.class)
  public UserSetting saveUserSetting(UserSettingResponse userSettingResponse) {
    Optional<Setting> wrapperSetting =
        settingRepository.findById(userSettingResponse.getSettingId());
    Optional<UserSetting> wrapperOldUserSetting =
        userSettingRepository.findByUserIdAndSettingId(
            userSettingResponse.getUserId(), userSettingResponse.getSettingId());
    UserSetting userSettingToSave = new UserSetting();
    if (wrapperSetting.isPresent()) {
      Setting setting = wrapperSetting.get();
      userSettingToSave = userSettingResponse.toUserSetting(setting);
    } else {
      throw new EntityNotFoundException(TABLE_NAME_SETTING, userSettingResponse.getSettingId());
    }
    if (wrapperOldUserSetting.isPresent()) {
      userSettingToSave = wrapperOldUserSetting.get();
      userSettingToSave.setValue(userSettingResponse.getValue());
    }
    try {
      UserSetting userSettingToSend = userSettingRepository.save(userSettingToSave);
      eventMessageService.sendUserSettingMessage(userSettingToSend);
      return userSettingToSend;
    } catch (ConstraintViolationException e) {
      throw new EntityUnableToStorageException(TABLE_NAME_USER_SETTING);
    }
  }
}
