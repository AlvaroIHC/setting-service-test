package org.fundacion_jala.setting_service.services;

import java.util.Date;
import org.fundacion_jala.setting_service.events.Event;
import org.fundacion_jala.setting_service.events.UserSettingCreationEvent;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.models.kafka.UserSettingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EventMessageService {

  private final String USER_SETTING_VALUE_ACTIVATED = "true";
  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${spring.kafka.topic.producer.name}")
  private String topicSetting;

  public void sendUserSettingMessage(UserSetting userSetting) {
    UserSettingDTO setting = new UserSettingDTO();
    setting.setUserId(userSetting.getUserId());
    setting.setValue(userSetting.getValue().equals(USER_SETTING_VALUE_ACTIVATED));
    setting.setSettingId(userSetting.getSetting().getId());
    UserSettingCreationEvent created = new UserSettingCreationEvent();
    created.setData(setting);
    created.setCreatedAt(new Date());
    producer.send(topicSetting, created);
  }
}
