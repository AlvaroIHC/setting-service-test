package org.fundacion_jala.setting_service.configurations;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:messageSwagger.properties")
@ConfigurationProperties(prefix = "value")
public class SwaggerConfig {

  @Value("${value.title}")
  String title;

  @Value("${value.description}")
  String description;

  @Value("${value.version}")
  String version;

  @Bean
  public OpenAPI api() {
    return new OpenAPI().info(new Info().title(title).description(description).version(version));
  }
}
