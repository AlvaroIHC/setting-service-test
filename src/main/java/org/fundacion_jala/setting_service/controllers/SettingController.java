package org.fundacion_jala.setting_service.controllers;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.fundacion_jala.setting_service.responses.SettingResponse;
import org.fundacion_jala.setting_service.responses.UserSettingResponse;
import org.fundacion_jala.setting_service.services.SettingService;
import org.fundacion_jala.setting_service.utils.constants.EndPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EndPoint.SETTINGS)
@CrossOrigin(origins = "${react.origin.url}")
public class SettingController {

  @Autowired private SettingService settingService;

  @GetMapping
  public ResponseEntity<List<SettingResponse>> findByUserIdAndSettingId(
      @RequestParam UUID roleId, @RequestParam UUID userId) {
    return ResponseEntity.ok(settingService.getSettingsByRoleIdAndUserId(roleId, userId));
  }

  @PostMapping
  public ResponseEntity<UserSetting> saveUserSetting(@RequestBody UserSettingResponse userSetting) {
    return ResponseEntity.ok(settingService.saveUserSetting(userSetting));
  }
}
