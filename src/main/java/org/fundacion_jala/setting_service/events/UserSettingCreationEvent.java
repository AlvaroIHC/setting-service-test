package org.fundacion_jala.setting_service.events;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.fundacion_jala.setting_service.models.kafka.UserSettingDTO;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserSettingCreationEvent extends Event<UserSettingDTO> {}
