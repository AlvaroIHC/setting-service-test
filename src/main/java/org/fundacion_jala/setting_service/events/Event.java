package org.fundacion_jala.setting_service.events;

import java.util.Date;
import lombok.Data;

@Data
public abstract class Event<T> {
  private Date createdAt;
  private T data;
}
