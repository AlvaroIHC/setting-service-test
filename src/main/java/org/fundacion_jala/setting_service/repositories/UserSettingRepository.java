package org.fundacion_jala.setting_service.repositories;

import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.UserSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSettingRepository extends JpaRepository<UserSetting, UUID> {
  Optional<UserSetting> findByUserIdAndSettingId(UUID userId, UUID settingId);
}
