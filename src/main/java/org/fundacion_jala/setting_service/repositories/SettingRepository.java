package org.fundacion_jala.setting_service.repositories;

import java.util.UUID;
import org.fundacion_jala.setting_service.models.Setting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettingRepository extends JpaRepository<Setting, UUID> {}
