package org.fundacion_jala.setting_service.repositories;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.setting_service.models.RoleSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleSettingRepository extends JpaRepository<RoleSetting, UUID> {
  public List<RoleSetting> findAllByRoleId(UUID uuid);
}
