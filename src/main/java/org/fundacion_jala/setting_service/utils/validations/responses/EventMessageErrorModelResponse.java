package org.fundacion_jala.setting_service.utils.validations.responses;

import lombok.Getter;
import lombok.Setter;

/** Model for HTTP response body about Kafka Exceptions */
@Getter
@Setter
public class EventMessageErrorModelResponse {

  private String message;
  private String description;
}
