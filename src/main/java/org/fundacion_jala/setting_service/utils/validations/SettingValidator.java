package org.fundacion_jala.setting_service.utils.validations;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SettingValidator {
  public static final String VALIDATION_LENGTH_VALUE = "{validation.setting.description.min_max}";
  public static final String NOT_NULL_MESSAGE = "{validation.setting.description.not_null}";
  public static final String NOT_BLANK_MESSAGE = "{validation.setting.description.notblank}";
  public static final String VALIDATION_PATTERN_DESCRIPTION =
      "{validation.setting.description.not_null}";
}
