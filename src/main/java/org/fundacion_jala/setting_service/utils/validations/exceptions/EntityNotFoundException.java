package org.fundacion_jala.setting_service.utils.validations.exceptions;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

  public EntityNotFoundException(String entityType, UUID id) {
    super(String.format("Unable to find %s with id %s", entityType, id));
  }

  public EntityNotFoundException(String entityType, String name) {
    super(String.format("Unable to find a %s with name %s", entityType, name));
  }
}
