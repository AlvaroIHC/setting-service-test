package org.fundacion_jala.setting_service.utils.validations.responses;

import lombok.Getter;
import lombok.Setter;

/** Field for an error model response */
@Getter
@Setter
public class FieldErrorModel {

  private String field;
  private String message;
  private String code;
}
