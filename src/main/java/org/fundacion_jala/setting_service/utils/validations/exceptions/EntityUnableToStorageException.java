package org.fundacion_jala.setting_service.utils.validations.exceptions;

public class EntityUnableToStorageException extends RuntimeException {

  public EntityUnableToStorageException(String entityType) {
    super(String.format("Unable to storage %s in database", entityType));
  }
}
