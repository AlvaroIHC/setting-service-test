package org.fundacion_jala.setting_service.utils.validations;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserSettingValidator {
  public static final String VALIDATION_LENGTH_VALUE =
      "{validation.user_setting.description.min_max}";
  public static final String NOT_NULL_MESSAGE = "{validation.user_setting.description.not_null}";
}
