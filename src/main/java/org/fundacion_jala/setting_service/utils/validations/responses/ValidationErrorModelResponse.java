package org.fundacion_jala.setting_service.utils.validations.responses;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/** Model for validation exceptions in Entities */
@Getter
@Setter
public class ValidationErrorModelResponse {

  private String message;
  private List<FieldErrorModel> errors;

  public ValidationErrorModelResponse() {
    this.errors = new ArrayList<>();
  }
}
