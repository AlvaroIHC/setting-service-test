package org.fundacion_jala.setting_service.utils.validations;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TypeSettingValidator {
  public static final String VALIDATION_LENGTH_DESCRIPTION =
      "{validation.type_setting.description.min_max}";
  public static final String NOT_NULL_MESSAGE = "{validation.type_setting.description.not_null}";
  public static final String NOT_BLANK_MESSAGE_NAME = "{validation.type_setting.name.notblank}";
  public static final String NOT_BLANK_MESSAGE_DESCRIPTION =
      "{validation.type_setting.description.notblank}";
  public static final String VALIDATION_PATTERN_NAME = "{validation.type_setting.name.pattern}";
  public static final String VALIDATION_PATTERN_DESCRIPTION =
      "{validation.setting.description.pattern}";
}
