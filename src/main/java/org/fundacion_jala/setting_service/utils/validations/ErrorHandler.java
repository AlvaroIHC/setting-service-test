package org.fundacion_jala.setting_service.utils.validations;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.kafka.common.errors.TimeoutException;
import org.fundacion_jala.setting_service.utils.constants.KafkaConstants;
import org.fundacion_jala.setting_service.utils.validations.exceptions.EntityNotFoundException;
import org.fundacion_jala.setting_service.utils.validations.exceptions.EntityUnableToStorageException;
import org.fundacion_jala.setting_service.utils.validations.responses.EventMessageErrorModelResponse;
import org.fundacion_jala.setting_service.utils.validations.responses.FieldErrorModel;
import org.fundacion_jala.setting_service.utils.validations.responses.ValidationErrorModelResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ValidationErrorModelResponse> handleInvalidValues(
      MethodArgumentNotValidException exception) {
    List<FieldErrorModel> errors =
        exception.getBindingResult().getAllErrors().stream()
            .map(
                currentError -> {
                  FieldErrorModel fieldErrorModel = new FieldErrorModel();
                  fieldErrorModel.setField(((FieldError) currentError).getField());
                  fieldErrorModel.setMessage(currentError.getDefaultMessage());
                  fieldErrorModel.setCode(currentError.getCode());
                  return fieldErrorModel;
                })
            .collect(Collectors.toList());
    ValidationErrorModelResponse response = new ValidationErrorModelResponse();
    response.setMessage(exception.getMessage());
    response.setErrors(errors);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ValidationErrorModelResponse> handleEntityNotFound(
      EntityNotFoundException exception) {
    ValidationErrorModelResponse response = new ValidationErrorModelResponse();
    response.setMessage(exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
  }

  @ExceptionHandler(TimeoutException.class)
  public ResponseEntity<EventMessageErrorModelResponse> handleKafkaTimeoutException(
      TimeoutException exception) {
    EventMessageErrorModelResponse response = new EventMessageErrorModelResponse();
    response.setMessage(exception.getMessage());
    response.setDescription(KafkaConstants.MESSAGE_EVENT_EXCEPTION_FAIL);
    return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
  }

  @ExceptionHandler(EntityUnableToStorageException.class)
  public ResponseEntity<ValidationErrorModelResponse> handleUnableToStorageException(
      EntityUnableToStorageException exception) {
    ValidationErrorModelResponse response = new ValidationErrorModelResponse();
    response.setMessage(exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
  }
}
