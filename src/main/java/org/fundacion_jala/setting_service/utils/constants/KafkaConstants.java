package org.fundacion_jala.setting_service.utils.constants;

public final class KafkaConstants {

  public static final String MESSAGE_EVENT_EXCEPTION_FAIL =
      "The message event could not be sent, data not persisted";
}
