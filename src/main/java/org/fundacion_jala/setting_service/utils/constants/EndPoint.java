package org.fundacion_jala.setting_service.utils.constants;

public class EndPoint {

  public static final String BASE = "/api/v1";

  public static final String SETTINGS = BASE + "/settings";
}
