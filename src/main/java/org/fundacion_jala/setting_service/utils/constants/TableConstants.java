package org.fundacion_jala.setting_service.utils.constants;

public final class TableConstants {
  public static final String TYPE_UUID = "uuid-char";
  public static final String TRAILING_FOREIGN_ID = "_id";
  public static final String TABLE_NAME_SETTING = "setting";
  public static final String TABLE_NAME_SETTING_ROLE = "setting_role";
  public static final String TABLE_NAME_TYPE_SETTING = "type_setting";
  public static final String TABLE_NAME_USER = "users";
  public static final String TABLE_NAME_ROLE = "roles";
  public static final String TABLE_NAME_USER_SETTING = "user_setting";
  public static final String TABLE_FOREIGN_TYPE_SETTING_ID =
      TABLE_NAME_TYPE_SETTING + TRAILING_FOREIGN_ID;
  public static final String TABLE_FOREIGN_ROLE_ID = TABLE_NAME_ROLE + TRAILING_FOREIGN_ID;
  public static final String TABLE_FOREIGN_SETTING_ID = TABLE_NAME_SETTING + TRAILING_FOREIGN_ID;
}
