package org.fundacion_jala.setting_service.utils.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Defaults {

  public static final int MIN_STRING_LENGTH = 3;
  public static final int MAX_STRING_LENGTH = 140;
  public static final int MAX_STRING_LENGTH_DESCRIPTION = 300;
  public static final String ALPHANUMERIC_CHARACTERS =
      "^[a-z A-Z0-9]([a-zA-Z0-; -\"'-\\)\\[\\]\\{\\}\t…,-.!_?]*)[a-z A-Z0-9.:!?]$";
  public static final String ERROR_MESSAGE_NAME = "Error";
  public static final String BAD_CREDENTIALS_ERROR = "Email or password incorrect";
  public static final String ERROR_CODE = "Code";
}
