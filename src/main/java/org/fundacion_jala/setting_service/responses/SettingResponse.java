package org.fundacion_jala.setting_service.responses;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.setting_service.models.SectionSetting;
import org.fundacion_jala.setting_service.models.enums.SettingDataType;

@Getter
@Setter
@AllArgsConstructor
public class SettingResponse {
  private UUID id;
  private SectionSetting typeSetting;
  private SettingDataType settingDataType;
  private String description;
  private boolean value;
}
