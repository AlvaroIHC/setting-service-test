package org.fundacion_jala.setting_service.responses;

import java.util.UUID;
import lombok.Data;
import org.fundacion_jala.setting_service.models.Setting;
import org.fundacion_jala.setting_service.models.UserSetting;

@Data
public class UserSettingResponse {
  private UUID userId;
  private String value;
  private UUID settingId;

  public UserSetting toUserSetting(Setting setting) {
    UserSetting userSetting = new UserSetting();
    userSetting.setUserId(userId);
    userSetting.setSetting(setting);
    userSetting.setValue(value);
    return userSetting;
  }
}
