-- Default Type --
INSERT IGNORE INTO `type_setting` (`id`, `description`, `name`) VALUES ('9b2bddd4-f719-11ec-b939-0242ac120002', 'Section notifications', 'Notification');

-- Default Settings --
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db6-f717-11ec-b939-0242ac120002', 'Notify when roles are added, updated or deleted', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db7-f717-11ec-b939-0242ac120002', 'Notify when an unauthorized user tries to get access', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db8-f717-11ec-b939-0242ac120002', 'Notify when a program has completed', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db9-f717-11ec-b939-0242ac120002', 'Notify for accomplishment for a scholar', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db0-f717-11ec-b939-0242ac120002', 'Notify for incident of a scholar', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db1-f717-11ec-b939-0242ac120002', 'Notify for informational data is created for a scholar', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting` (`id`, `description`, `setting_data_type`, `type_setting_id`) VALUES ('40d56db2-f717-11ec-b939-0242ac120002', 'Notify when a new user is added', 'BOOLEAN', '9b2bddd4-f719-11ec-b939-0242ac120002');
-- Default Settings_Rol --
--Setting 1--
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936aa56-f719-11ec-b939-0242ac120002', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', '40d56db6-f717-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4a-f719-11ec-b939-0242ac120002', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', '40d56db6-f717-11ec-b939-0242ac120002');
--Setting 2--
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4b-f719-11ec-b939-0242ac120002', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', '40d56db7-f717-11ec-b939-0242ac120002');
--Setting 3--
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4c-f719-11ec-b939-0242ac120002', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', '40d56db8-f717-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4d-f719-11ec-b939-0242ac120002', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', '40d56db8-f717-11ec-b939-0242ac120002');
--Setting 4 5 6--
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4e-f719-11ec-b939-0242ac120002', '045e9370-0dc9-4665-9a89-1435eb7071b5', '40d56db9-f717-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4f-f719-11ec-b939-0242ac120002', '045e9370-0dc9-4665-9a89-1435eb7071b5', '40d56db0-f717-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('6fb69f5c-f829-11ec-b939-0242ac120002', '045e9370-0dc9-4665-9a89-1435eb7071b5', '40d56db1-f717-11ec-b939-0242ac120002');
--Setting 7 --
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936aa57-f719-11ec-b939-0242ac120002', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', '40d56db2-f717-11ec-b939-0242ac120002');
INSERT IGNORE INTO `setting_role` (`id`, `role_id`, `setting_id`) VALUES ('c936ac4g-f719-11ec-b939-0242ac120002', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', '40d56db2-f717-11ec-b939-0242ac120002');