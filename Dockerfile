FROM maven:3.8.3-openjdk-8-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -Dmaven.test.skip=true

FROM openjdk:8-jdk-alpine
ARG JAR_FILE=home/app/target/*.jar
COPY --from=build ${JAR_FILE} application.jar
COPY run.sh run.sh
RUN  apk update && apk upgrade && apk add netcat-openbsd
RUN chmod +x run.sh
CMD ./run.sh